{
  const { Element, mixinBehaviors, IronMenuBehavior } = Polymer;

  /**
   * `<cells-select-list>` option item inside `<cells-select>`.
   *
   * ## Styling
   *
   * The following custom properties and mixins are available for styling:
   *
   *
   * ### Custom Properties
   * | Custom Property                         | Selector                  | CSS Property                     | Value                                   |
   * | --------------------------------------- | ------------------------- | -------------------------------- | --------------------------------------- |
   * | --cells-select-list-color               | :host                     | --cslprivate-color               | --bbva-500                              |
   * | --cells-select-list-border-bottom-color | :host                     | --cslprivate-border-bottom-color | --bbva-300                              |
   * | --cells-select-list-selected-color      | :host                     | --cslprivate-selected-color      | --bbva-600                              |
   * | --cells-select-list-selected-bg-icon    | :host                     | --cslprivate-selected-bg-icon    | ![image](checkmark-medium-blue.svg)     |
   * | --cslprivate-border-bottom-color        | ::slotted(*)              | border-bottom                    | --cells-select-list-border-bottom-color |
   * | --cslprivate-color                      | ::slotted(*)              | color                            | --cells-select-list-color               |
   * | --cslprivate-selected-color             | ::slotted(.iron-selected) | color                            | --cells-select-list-selected-color      |
   * | --cslprivate-selected-color             | ::slotted(:focus)         | color                            | --cells-select-list-selected-color      |
   * | --cslprivate-selected-color             | ::slotted(:focus)         | border-bottom-color              | --cells-select-list-selected-color      |
   * | --cslprivate-selected-color             | ::slotted(:hover)         | color                            | --cells-select-list-selected-color      |
   * ### @apply
   * | Mixins                            | Selector                  | Value |
   * | --------------------------------- | ------------------------- | ----- |
   * | --cells-select-list               | :host                     | {}    |
   * | --cells-select-list-item          | ::slotted(*)              | {}    |
   * | --cells-select-list-item-selected | ::slotted(.iron-selected) | {}    |
   * | --cells-select-list-item-focused  | ::slotted(:focus)         | {}    |
   * | --cells-select-list-item-hover    | ::slotted(:hover)         | {}    ||
   *
   * @summary
   * @customElement
   * @polymer
   * @appliesMixin Polymer.IronMenuBehavior
   * @extends {Polymer.Element}
   */
  class CellsSelectList extends mixinBehaviors([ IronMenuBehavior ], Element) {
    static get is() {
      return 'cells-select-list';
    }
  }

  window.customElements.define(CellsSelectList.is, CellsSelectList);
}
