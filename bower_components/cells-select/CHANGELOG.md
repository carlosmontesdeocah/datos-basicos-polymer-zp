# Changelog

## v11.0.0 - 2018-07

### Rewritten from scratch

- Migrated to __Polymer 2 (<em>Class based</em>)__ with [extension points](https://www.polymer-project.org/2.0/docs/devguide/dom-template#inherit) for easy extension / customization.
- Several fixes related to selection and keyboard navigation.
- The keyboard functionality is managed using [IronMenuBehavior](https://www.webcomponents.org/element/PolymerElements/iron-menu-behavior).
- Usability and API improvements:
  - The list of options can be set in different manners (using light DOM or an `options` property).
  - New `selected` property that allows to set the selected option.
  - New `value` property that returns the value of the selected option.  
- Accessibility improvements.
- Design adapted to [UI Studio](https://ui.bbvaexperience.com/web/dropdowns/).

### Breaking changes

- Requires Polymer 2.

__Removed properties:__

- `rightIcon` in favour of `icon`
- `disabledFirstSelectedByDefault`
- `focusOnClose` in favour of `noFocusOnClose` (opposite behavior).

__Removed methods:__

- `toggle()` in favour of `opened` property.
- `close()` in favour of `opened` property.
- `selectOption()` in favour of `selected` property.
- `resetSelection()` `selected` property can be used instead.

__Removed events:__

- `select-item-selected` `selected` property notifies and can be used instead of the previous event.

__CSS custom properties and mixins:__

All the previous properties can be considered outdated even if the name have not changed since they are not applied to the same internal elements.


## v0.3.2 - 2016-03-14
### Added
- feat(js): Added reset method to initialize dropdown from outside.

## v0.3.0 - 2016-03-09
### Added
- feat(js): Fixed functionality of closing dropdown options list when dropdown is opened and user clicks outside the opened dropdown

### Fixes
- fix(js): Fixed open-close management when one dropdown is opened and user clicks in another. OK for production
- fix(js): fixed bug with eventListener when there were no opened dropdown
