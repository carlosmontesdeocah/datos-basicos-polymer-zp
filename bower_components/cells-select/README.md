![cells-select screenshot](cells-select.png)

# cells-select

![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg)
![Polymer 2.x](https://img.shields.io/badge/Polymer-2.x-green.svg)

[Demo of component in Cells Catalog](https://catalogs.platform.bbva.com/cells)

`<cells-select>` is a custom `<select>` element.

It admits multiple formats for its list of options:

- HTML nodes in light DOM.
- Simple array (list of texts) as `options`.
- Complex array (list of objects) as `options`.
- Both, items in light DOM and `options` property combined. Items in light DOM will be shown after the items passed in `options`.

The selected value is available in the `value` (readOnly) property.

The selected item can be set using the `selected` property with one index of the options.

## Usage

__With options in light DOM:__

```html
<cells-select label="City">
  <div>Madrid</div>
  <div>Bilbao</div>
  <div>Barcelona</div>
  <div>Logroño</div>
</cells-select>
```

__With simple array of options:__

```html
<cells-select label="City" options='["Madrid", "Bilbao", "Barcelona", "Logroño"]'></cells-select>
```

__With complex array of options:__

When using objects as options, the `label` property should be provided for each option. The `value` will be set with the same value that `label` if not provided.

```html
<cells-select label="City"
  options='[{
    "label": "Madrid",
    "value": "city-1"
  }, {
    "label": "Barcelona",
    "value": "city-2"
  }]'></cells-select>
```

### Data model

__Using `options` as an array of objects:__

When using a complex array (objects) as `options`, the properties for each option will be used as the model and will be available in the `selectedOption` property.

In the following example, `selectedOption` will be an object with `label`, `value` and `airportCode` properties.

```html
<cells-select label="City"
  selected-option="{{selectedOption}}"
  options='[{
    "label": "Madrid",
    "value": "city-1",
    "airportCode": "MAD"
  }, {
    "label": "Barcelona",
    "value": "city-2",
    "airportCode": "BCN"
  }]'></cells-select>
```

__Using HTML nodes in light DOM:__

When using nodes in light DOM, a data model can be provided by setting `data-*` attributes to each node. Data attributes starting with `data-option-` will be used as the model for the `selectedOption` property.

Note that the `data-option-value` can be used to set a different value for the selected option that the `innerText` (default when using options in light DOM).

In the following example, `selectedOption` will be an object with `value` and `airportCode` properties.

```html
<cells-select label="City" selected-option="{{selectedOption}}">
  <div data-option-value="city-1" data-option-airport-code="MAD">Madrid</div>
  <div data-option-value="city-2" data-option-airport-code="BIO">Bilbao</div>
  <div data-option-value="city-3" data-option-airport-code="BCN">Barcelona</div>
  <div data-option-value="city-4" data-option-airport-code="RJL">Logroño</div>
</cells-select>
```

### Template extension

The component uses [extension points](https://www.polymer-project.org/2.0/docs/devguide/dom-template#inherit) that can be overriden from a component extending `<cells-select>`.

The available extension points are:

- `template`: contains the styles, content (toggle list and list of options) and the error message.
- `toggleTemplate`: contains the toggle (clicable area with the label, placeholder, selected text and toggle icon).
- `optionTemplate`: contains the HTML for each of the options when using `options` property.
- `customStyles`: an empty tagged template literal intended to add an additional style tag after the own component styles.

Check out __demo/extended-select.html__ for an example.

## Styling

The following custom properties and mixins are available for styling:

### Custom Properties
| Custom Property                          | Selector                                           | CSS Property                          | Value                                                            |
| ---------------------------------------- | -------------------------------------------------- | ------------------------------------- | ---------------------------------------------------------------- |
| --cells-fontDefault                      | :host                                              | font-family                           | sans-serif                                                       |
| --cells-select-bg-color                  | :host                                              | --csprivate-bg-color                  | --bbva-100                                                       |
| --cells-select-faded-text-color          | :host                                              | --csprivate-faded-text-color          | --bbva-500                                                       |
| --cells-select-text-color                | :host                                              | --csprivate-text-color                | --bbva-600                                                       |
| --cells-select-hover-border-color        | :host                                              | --csprivate-hover-border-color        | --bbva-400                                                       |
| --cells-select-focus-shadow-color        | :host                                              | --csprivate-focus-shadow-color        | --bbva-dark-medium-blue                                          |
| --cells-select-toggle-icon-color         | :host                                              | --csprivate-toggle-icon-color         | --bbva-medium-blue                                               |
| --cells-select-error-icon-color          | :host                                              | --csprivate-error-icon-color          | --bbva-dark-red                                                  |
| --cells-select-faded-text-color          | :host(.core-dark-blue)                             | --csprivate-faded-text-color          | --bbva-200                                                       |
| --cells-select-text-color                | :host(.core-dark-blue)                             | --csprivate-text-color                | --bbva-white                                                     |
| --cells-select-hover-border-color        | :host(.core-dark-blue)                             | --csprivate-hover-border-color        | --bbva-white                                                     |
| --cells-select-focus-shadow-color        | :host(.core-dark-blue)                             | --csprivate-focus-shadow-color        | --bbva-white                                                     |
| --cells-select-toggle-icon-color         | :host(.core-dark-blue)                             | --csprivate-toggle-icon-color         | --bbva-white                                                     |
| --bbva-light-coral                       | :host(.core-dark-blue)                             | --cells-select-error-icon-color       | ![#F59799](https://placehold.it/15/F59799/000000?text=+) #F59799 |
| --bbva-200                               | :host(.core-dark-blue:not([error])) .list          | --cells-select-list-color             | ![#E9E9E9](https://placehold.it/15/E9E9E9/000000?text=+) #E9E9E9 |
| --bbva-white                             | :host(.core-dark-blue:not([error])) .list          | --cells-select-list-selected-color    | ![#FFFFFF](https://placehold.it/15/FFFFFF/000000?text=+) #FFFFFF |
| --cells-select-text-color                | :host(.core-dark-blue[error]) .toggle__placeholder | --csprivate-text-color                | --bbva-600                                                       |
| --cells-select-bg-color                  | :host(.white)                                      | --csprivate-bg-color                  | --bbva-white                                                     |
| --cells-select-bg-color                  | :host(.medium-blue)                                | --csprivate-bg-color                  | --bbva-medium-blue                                               |
| --cells-select-bg-color                  | :host(.core-blue)                                  | --csprivate-bg-color                  | --bbva-dark-medium-blue                                          |
| --cells-select-bg-color                  | :host(.core-dark-blue)                             | --csprivate-bg-color                  | --bbva-core-blue                                                 |
| --csprivate-hover-border-color           | :host(:hover) .toggle                              | border-color                          | --cells-select-hover-border-color                                |
| --csprivate-faded-text-color             | :host(:hover) .toggle                              | border-bottom-color                   | --cells-select-faded-text-color                                  |
| --csprivate-focus-shadow-color           | :host([focus-within]) .content                     | box-shadow                            | 0 0 1px --cells-select-focus-shadow-color                        |
| --csprivate-text-color                   | :host([focus-within]) .toggle                      | border-bottom-color                   | --cells-select-text-color                                        |
| --csprivate-focus-shadow-color           | :host([focus-within]) .list                        | box-shadow                            | 0 0 1px --cells-select-focus-shadow-color                        |
| --csprivate-faded-text-color             | :host([readonly]) .toggle                          | border-bottom-color                   | --cells-select-faded-text-color                                  |
| --cells-select-disabled-opacity          | :host([disabled])                                  | opacity                               | 0.3                                                              |
| --csprivate-faded-text-color             | :host([readonly]) .toggle__label                   | color                                 | --cells-select-faded-text-color                                  |
| --csprivate-faded-text-color             | :host([readonly]) .toggle__placeholder             | color                                 | --cells-select-faded-text-color                                  |
| --csprivate-faded-text-color             | :host([readonly]) .toggle__icon                    | color                                 | --cells-select-faded-text-color                                  |
| --csprivate-error-icon-color             | :host([error])                                     | --cells-icon-message-icon-color-error | --cells-select-error-icon-color                                  |
| --cells-select-error-bg-color            | :host([error]) .content                            | background-color                      | --bbva-white-coral                                               |
| --cells-select-error-border-bottom-color | :host([error]) .toggle                             | border-bottom-color                   | --bbva-dark-red                                                  |
| --cells-select-error-label-color         | :host([error]) .toggle__label                      | color                                 | --bbva-red                                                       |
| --cells-select-error-toggle-icon-color   | :host([error]) .toggle__icon                       | color                                 | --bbva-dark-red                                                  |
| --csprivate-bg-color                     | .content                                           | background-color                      | --cells-select-bg-color                                          |
| --csprivate-faded-text-color             | .toggle                                            | border-bottom-color                   | --cells-select-faded-text-color                                  |
| --csprivate-faded-text-color             | .toggle__label                                     | color                                 | --cells-select-faded-text-color                                  |
| --csprivate-text-color                   | .toggle__placeholder                               | color                                 | --cells-select-text-color                                        |
| --csprivate-toggle-icon-color            | .toggle__icon                                      | color                                 | --cells-select-toggle-icon-color                                 |
| --csprivate-text-color                   | .error-message                                     | color                                 | --cells-select-text-color                                        |
| --cells-select-list-color                | :host                                              | --cslprivate-color                    | --bbva-500                                                       |
| --cells-select-list-border-bottom-color  | :host                                              | --cslprivate-border-bottom-color      | --bbva-300                                                       |
| --cells-select-list-selected-color       | :host                                              | --cslprivate-selected-color           | --bbva-600                                                       |
| --cells-select-list-selected-bg-icon     | :host                                              | --cslprivate-selected-bg-icon         | ![image] url)                                                    |
| --cslprivate-border-bottom-color         | ::slotted(*)                                       | border-bottom                         | 1px solid --cells-select-list-border-bottom-color                |
| --cslprivate-color                       | ::slotted(*)                                       | color                                 | --cells-select-list-color                                        |
| --cslprivate-selected-color              | ::slotted(.iron-selected)                          | color                                 | --cells-select-list-selected-color                               |
| --cslprivate-selected-color              | ::slotted(:focus)                                  | color                                 | --cells-select-list-selected-color                               |
| --cslprivate-selected-color              | ::slotted(:focus)                                  | border-bottom-color                   | --cells-select-list-selected-color                               |
| --cslprivate-selected-color              | ::slotted(:hover)                                  | color                                 | --cells-select-list-selected-color                               |                                       
### @apply
| Mixins                                              | Selector                                     | Value |
| --------------------------------------------------- | -------------------------------------------- | ----- |
| --cells-select                                      | :host                                        | {}    |
| --cells-select-focus-content                        | :host([focus-within]) .content               | {}    |
| --cells-select-focus-toggle                         | :host([focus-within]) .toggle                | {}    |
| --cells-select-focus-list                           | :host([focus-within]) .list                  | {}    |
| --cells-select-opened-toggle-icon                   | :host([opened]) .toggle__icon                | {}    |
| --cells-select-opened-label-with-no-selection       | :host([opened]) .toggle__label--no-selection | {}    |
| --cells-select-disabled                             | :host([readonly])                            | {}    |
| --cells-select-disabled-toggle                      | :host([readonly]) .toggle                    | {}    |
| --cells-select-readonly-toggle                      | :host([readonly]) .toggle__label             | {}    |
| --cells-select-readonly-toggle                      | :host([readonly]) .toggle__placeholder       | {}    |
| --cells-select-readonly-toggle                      | :host([readonly]) .toggle__icon              | {}    |
| --cells-select-error                                | :host([error])                               | {}    |
| --cells-select-error-content                        | :host([error]) .content                      | {}    |
| --cells-select-error-toggle                         | :host([error]) .toggle                       | {}    |
| --cells-select-error-toggle-label                   | :host([error]) .toggle__label                | {}    |
| --cells-select-error-toggle-icon                    | :host([error]) .toggle__icon                 | {}    |
| --cells-select-content                              | .content                                     | {}    |
| --cells-select-toggle                               | .toggle                                      | {}    |
| --cells-select-toggle-focus                         | .toggle:focus                                | {}    |
| --cells-select-toggle-text                          | .toggle__text                                | {}    |
| --cells-select-toggle-label                         | .toggle__label                               | {}    |
| --cells-select-toggle-label-with-no-selection       | .toggle__label--no-selection                 | {}    |
| --cells-select-toggle-placeholder-with-no-selection | .toggle__label:empty + .toggle__placeholder  | {}    |
| --cells-select-toggle-placeholder                   | .toggle__placeholder                         | {}    |
| --cells-select-toggle-icon                          | .toggle__icon                                | {}    |
| --cells-select-options-list                         | .list                                        | {}    |
| --cells-select-error-message                        | .error-message                               | {}    |
| --cells-select-list                                 | :host                                        | {}    |
| --cells-select-list-item                            | ::slotted(*)                                 | {}    |
| --cells-select-list-item-selected                   | ::slotted(.iron-selected)                    | {}    |
| --cells-select-list-item-focused                    | ::slotted(:focus)                            | {}    |
| --cells-select-list-item-hover                      | ::slotted(:hover)                            | {}    |
## Styling

The following custom properties and mixins are available for styling:

### Custom Properties
| Custom Property                         | Selector                  | CSS Property                     | Value                                   |
| --------------------------------------- | ------------------------- | -------------------------------- | --------------------------------------- |
| --cells-select-list-color               | :host                     | --cslprivate-color               | --bbva-500                              |
| --cells-select-list-border-bottom-color | :host                     | --cslprivate-border-bottom-color | --bbva-300                              |
| --cells-select-list-selected-color      | :host                     | --cslprivate-selected-color      | --bbva-600                              |
| --cells-select-list-selected-bg-icon    | :host                     | --cslprivate-selected-bg-icon    | ![image](checkmark-medium-blue.svg)     |
| --cslprivate-border-bottom-color        | ::slotted(*)              | border-bottom                    | --cells-select-list-border-bottom-color |
| --cslprivate-color                      | ::slotted(*)              | color                            | --cells-select-list-color               |
| --cslprivate-selected-color             | ::slotted(.iron-selected) | color                            | --cells-select-list-selected-color      |
| --cslprivate-selected-color             | ::slotted(:focus)         | color                            | --cells-select-list-selected-color      |
| --cslprivate-selected-color             | ::slotted(:focus)         | border-bottom-color              | --cells-select-list-selected-color      |
| --cslprivate-selected-color             | ::slotted(:hover)         | color                            | --cells-select-list-selected-color      |
### @apply
| Mixins                            | Selector                  | Value |
| --------------------------------- | ------------------------- | ----- |
| --cells-select-list               | :host                     | {}    |
| --cells-select-list-item          | ::slotted(*)              | {}    |
| --cells-select-list-item-selected | ::slotted(.iron-selected) | {}    |
| --cells-select-list-item-focused  | ::slotted(:focus)         | {}    |
| --cells-select-list-item-hover    | ::slotted(:hover)         | {}    |
