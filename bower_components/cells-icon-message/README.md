![cells-icon-message screenshot](cells-icon-message.png)

# cells-icon-message

![Certificated](https://img.shields.io/badge/certificated-yes-brightgreen.svg) ![Polymer 2.x](https://img.shields.io/badge/Polymer-2.x-green.svg)

[Demo of component in Cells Catalog](https://bbva-ether-cellscatalogs.appspot.com/?view=demo#/component/cells-icon-message)

`<cells-icon-message>` displays an inline informative message, such an error, warning, info or success message with an icon.
It has two different layouts. The default layout displays the icon above the text and the "inline" layout displays the icon
to the left of the text.

The `aria-label` of the icon is set automatically for three of the default valid types (info, warning and error), but you can override that value by setting the
`icon-label` attribute. If no `icon-label` is set and none of the valid types is used, the icon will not have `aria-label` as it will be considered presentational.

An image can be used instead of an icon by setting the `src` attribute. The image will be square.

Inline level HTML tags are allowed for the message. Also custom content can be used as light DOM.

__Example:__

```html
<cells-icon-message
  type="info"
  icon="coronita:info"
  message="Informative message">
</cells-icon-message>
```

__Example with custom label for the icon:__

```html
<cells-icon-message
  type="warning"
  icon-label="Important message"
  icon="coronita:alert"
  message="Lorem ipsum dolor sit amet.">
</cells-icon-message>
```

__Example with an image used as icon:__

```html
<cells-icon-message
  src="http://domain/image.png"
  message="Lorem ipsum dolor sit amet.">
</cells-icon-message>
```

__Example with format (inline level tags):__

```html
<cells-icon-message
  message="Lorem <i>ipsum</i> dolor sit amet.">
</cells-icon-message>
```

```html
<cells-icon-message
  type="info"
  icon="coronita:info"
  heading="Heading Title"
  message="Informative message">
</cells-icon-message>
```
__Example using i18n Behavior:__

```html
<cells-icon-message
  type="info"
  icon="coronita:info"
  heading="cells-icon-message-title"
  message="cells-icon-message-message">
</cells-icon-message>
```

Translation on `'locales/es.json'`

```json
{
  "cells-icon-message-title": "Hey there!",
  "cells-icon-message-message": "I am a english message, you can use i18 behavior for translation purposes."
}
```

## Icons

Since this component uses icons, it will need an [iconset](https://bbva.cellsjs.com/guides/best-practices/cells-icons.html) in your project as an [application level dependency](https://bbva.cellsjs.com/guides/advanced-guides/application-level-dependencies.html).
In fact, this component uses an iconset in its demo.

## Styling

The following custom properties and mixins are available for styling:

### Custom Properties
| Custom Property                               | Selector                      | CSS Property     | Value                                                            |
| --------------------------------------------- | ----------------------------- | ---------------- | ---------------------------------------------------------------- |
| --cells-icon-message-icon-margin              | :host                         | --icon-margin    | 1.25rem                                                          |
| --cells-icon-message-header-margin            | :host                         | --header-margin  | 1.25rem                                                          |
| --cells-icon-message-header-font              | :host                         | --header-font    | 1rem                                                             |
| --cells-fontDefault                           | :host                         | font-family      | sans-serif                                                       |
| --cells-icon-message-color                    | :host                         | color            | ![#121212](https://placehold.it/15/121212/000000?text=+) #121212 |
| --cells-icon-message-background-color-info    | :host([type="info"])          | background-color | ![#D4EDFC](https://placehold.it/15/D4EDFC/000000?text=+) #D4EDFC |
| --cells-icon-message-background-color-warning | :host([type="warning"])       | background-color | ![#fde7d8](https://placehold.it/15/fde7d8/000000?text=+) #fde7d8 |
| --cells-icon-message-background-color-error   | :host([type="error"])         | background-color | ![#fcdfdf](https://placehold.it/15/fcdfdf/000000?text=+) #fddfdf |
| --cells-icon-message-background-color-success | :host([type="success"])       | background-color | ![#daefe0](https://placehold.it/15/daefe0/000000?text=+) #daefe0 |
| --cells-icon-message-icon-color               | .icon                         | color            | inherit                                                          |
| --icon-margin                                 | .icon                         | margin-bottom    | `No fallback value`                                              |
| --icon-margin                                 | :host(.inline) .icon          | margin-right     | calc( / 2)                                                       |
| --cells-icon-message-icon-color-info          | :host([type="info"]) .icon    | color            | ![#5BBEFF](https://placehold.it/15/5BBEFF/000000?text=+) #5BBEFF |
| --cells-icon-message-icon-color-warning       | :host([type="warning"]) .icon | color            | ![#fab27f](https://placehold.it/15/fab27f/000000?text=+) #fab27f |
| --cells-icon-message-icon-color-error         | :host([type="error"]) .icon   | color            | ![#f79698](https://placehold.it/15/f79698/000000?text=+) #f79698 |
| --cells-icon-message-icon-color-success       | :host([type="success"]) .icon | color            | ![#48ae64](https://placehold.it/15/48ae64/000000?text=+) #48ae64 |
| --header-margin                               | .header                       | margin-bottom    | `No fallback value`                                              |
| --header-font                                 | .header                       | font-size        | `No fallback value`                                              |
### @apply
| Mixins                              | Selector                       | Value |
| ----------------------------------- | ------------------------------ | ----- |
| --cells-icon-message                | :host                          | {}    |
| --cells-icon-message-info           | :host([type="info"])           | {}    |
| --cells-icon-message-warning        | :host([type="warning"])        | {}    |
| --cells-icon-message-error          | :host([type="error"])          | {}    |
| --cells-icon-message-success        | :host([type="success"])        | {}    |
| --cells-icon-message-inline         | :host(.inline)                 | {}    |
| --cells-icon-message-inline-info    | :host(.inline[type="info"])    | {}    |
| --cells-icon-message-inline-warning | :host(.inline[type="warning"]) | {}    |
| --cells-icon-message-inline-error   | :host(.inline[type="error"])   | {}    |
| --cells-icon-message-inline-success | :host(.inline[type="success"]) | {}    |
| --cells-icon-message-icon           | .icon                          | {}    |
| --cells-icon-message-icon-inline    | :host(.inline) .icon           | {}    |
| --cells-icon-message-icon-info      | :host([type="info"]) .icon     | {}    |
| --cells-icon-message-icon-warning   | :host([type="warning"]) .icon  | {}    |
| --cells-icon-message-icon-error     | :host([type="error"]) .icon    | {}    |
| --cells-icon-message-icon-success   | :host([type="success"]) .icon  | {}    |
| --cells-icon-message-message        | .message                       | {}    |
| --cells-icon-message-header         | .header                        | {}    |
