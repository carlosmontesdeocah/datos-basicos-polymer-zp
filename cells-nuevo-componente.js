{
  /**
    `<cells-nuevo-componente>` Description.

    Example:

    ```html
    <cells-nuevo-componente></cells-nuevo-componente>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-nuevo-componente | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsNuevoComponente extends Polymer.Element {

    static get is() {
      return 'cells-nuevo-componente';
    }

    static get properties() {
      return {
        tipoDocumento: {
          type: String,
          value: ''
        },
        numDocumento: {
          type: String,
          value: ''
        },
        correo: {
          type: String,
          value: ''
        },
        celular: {
          type: String,
          value: ''
        }
      };
    }

    constructor() {
      super();
    }

    solicitar() {
      console.log(`llego los datos: ${this.tipoDocumento.value}, ${this.numDocumento}, ${this.correo}, ${this.celular}`);
    }
  }

  customElements.define(CellsNuevoComponente.is, CellsNuevoComponente);
}