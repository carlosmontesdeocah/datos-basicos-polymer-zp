/**
 * `<cells-grid-template>` is a Polymer Style Module (Shared Style) with classes to distribute elements in a grid.
 * It's intended to be used in template components or _first level_ components (page compositions) but not inside standalone
 * components because __class names are relative to the number of columns in a page__ using an specific grid (eg.: 12 columns, 16 columns, etc.).
 *
 * ## Features
 *
 * - Based in flexbox.
 * - Every column has half of the gutter width (32px) as lateral margins (16px).
 * - The gutter width can be customized using the Custom CSS Property `--cells-grid-template-gutter-width`.
 * - The gutter width can be different for each breakpoint (media query).
 * - No need to use row containers.
 *
 * ## Default settings
 *
 * | Class prefix   | Media Query                    | Columns | Gutter width | Max width |
 * | :------------- | :----------------------------- | ------: | -----------: | --------: |
 * | `.cgt-col-xs-` | max-width: 37.4375em (< 600px) |       4 |         32px |         - |
 * | `.cgt-col-sm-` | min-width: 37.5em (>= 600px)   |       8 |         32px |         - |
 * | `.cgt-col-md-` | min-width: 62em (>= 992px)     |      12 |         32px |     960px |
 * | `.cgt-col-lg-` | min-width: 75em (>= 1200px)    |      12 |         32px |    1176px |
 *
 * ## Usage
 *
 * The Style Module needs to be imported by the template or _first level_ component as a dependency (not devDependency).
 *
 * Example in `my-template.html`:
 *
 * ```html
 * <link rel="import" href="../cells-grid-template/cells-grid-template-styles.html">
 *
 * <dom-module id="my-template">
 *   <template>
 *     <style include="cells-grid-template-styles"></style>
 *   </template>
 * </dom-module>
 * ```
 *
 * Once the component is imported, you can use cells-grid-template classes inside your component:
 *
 * ```html
 * <dom-module id="my-template">
 *   <template>
 *     <style include="cells-grid-template-styles"></style>
 *   </template>
 *
 *   <div class="cgt">
 *     <div class="cgt-col-xs-2">…</div>
 *     <div class="cgt-col-md-1">…</div>
 *   </div>
 * </dom-module>
 * ```
 *
 * __To ensure the grid to work in Safari 9.X, you need to initialize the gutter width:__
 *
 * ```html
 * <dom-module id="my-template">
 *   <template>
 *     <style include="cells-grid-template-styles">
 *       :host {
 *         --cells-grid-template-gutter-width: 32px;
 *       }
 *     </style>
 *   </template>
 *   ...
 * </dom-module>
 * ```
 *
 * __Internet Explorer did not support css variables, so the gutter is set by default to 32px and can not be changed__
 * 
 * ## Classes
 *
 * - Grid container: `.cgt`
 * - Grid columns: `.cgt-col-<device>-<number>` (Eg.: `.cgt-col-xs-1`)
 *
 * ### Columnn margins (pull and push)
 *
 * Column grids can have left and/or right margin equal to a number of columns. This is useful to place an element in the 3rd column, for example, or to move an element to the next row.
 * The syntax is as follows, where the `<number>` refers to the number of columns:
 *
 * `.cgt-col-<device>-ml/mr-<number>`
 *
 * Examples:
 *
 * - `.cgt-col-xs-ml-2`
 * - `.cgt-col-lg-mr-1`
 *
 * To __reset margins__ to the default values use 0 as number:
 *
 * - `.cgt-col-xs-ml-0`
 * - `.cgt-col-xs-mr-0`
 * - `.cgt-col-xs-m-0` (left and right margin)
 *
 * ## Debug
 *
 * Add the class `grid--debug` to the grid container to see the limits of the grid columns.
 *
 * Example:
 *
 * ```html
 * <div class="cgt grid--debug">
 *   <div class="cgt-col-xs-1"></div>
 * </div>
 * ```
 *
 * The element `<cells-grid-template-overlay>` displays an overlay with the grid columns.
 * You can show or hide the overlay using the class `.hidden` or the `hidden` attribute. Check out the demo to see an example.
 *
 * ```html
 * <link rel="import" href="../cells-grid-template/cells-grid-template-overlay.html">
 * …
 * <div class="cgt">
 *   <div class="cgt-col-xs-1"></div>
 * </div>
 *
 * <cells-grid-template-overlay class="hidden"></cells-grid-template-overlay>
 * ```
 *
 * ## Customization
 *
 * The gutter width can be customized for all the breakpoints or for specific ones.
 *
 * Example:
 *
 * ```html
 * <style include="cells-grid-template-styles">
 *   .cgt {
 *     // override gutter width for all breakpoints
 *     --cells-grid-template-gutter-width: 40px;
 *   }
 *
 *   @media screen and (min-width: 75em) {
 *     .cgt {
 *       // override gutter width for this breakpoint
 *       --cells-grid-template-gutter-width: 10px;
 *     }
 *   }
 * </style>
 * ```
 *
 * ## Creating your own grids
 *
 * You can create your own grid with different settings (breakpoints, columns, class names, etc.) by importing the sass file and overriding the default var and map values.
 *
 * Example in `my-custom-grid.scss`:
 *
 * ```css
 * $grid-class: "my-grid";
 * $grid-col-class: "col";
 *
 * $max-width-includes-margin: false; // default value is true
 *
 * $breakpoints: (
 *   xs: (
 *     max: 37.4375em,
 *     columns: 4
 *   ),
 *   sm: (
 *     min: 37.5em,
 *     columns: 8
 *   ),
 *   md: (
 *     min: 62em,
 *     columns: 12,
 *     max-width: 960px
 *   ),
 *   lg: (
 *     min: 75em,
 *     columns: 12,
 *     max-width: 1176px
 *   )
 * );
 *
 * // import cells-grid-template.scss after the vars and breakpoints map definition
 * @import "bower_components/cells-grid-template/cells-grid-template";
 * ```
 *
 * You can find a file called `custom-grid-SAMPLE.txt` in this repo with the configuration vars and map.
 *
 * @polymer
 * @pseudoElement cells-grid-template
 * @extends {Polymer.Element}
 * @summary Style Module with CSS classes to distribute elements in a grid.
 * @customElement
 * @demo demo/index.html
 * @hero cells-grid-template.png
 */
class CellsGridTemplateOverlay extends Polymer.Element {
  static get is() {
    return 'cells-grid-template-overlay';
  }
}
window.customElements.define(CellsGridTemplateOverlay.is, CellsGridTemplateOverlay);